#include<iostream>
#include<iomanip>
#include<fstream>
#include<unordered_set>
#include<unordered_map>
#include<string>
#include<set>
#include<vector>
#include<stack>
#include<queue>
#include<map>
#include <cstdio>
#include <windows.h>
#define TERMINS 20000 //终结符从20000以后开始编号
#define NONTERMINS 30000 //非终结符从30000以后开始编号
#define POUND 19999//#号标记为19999
#define SSTART 29999//拓广文法开始符号S'标记为29999
#define EPSILON 39999//产生式里可能会出现的空,平时标记为ε
#define ERROR 40000//错误边，用于占位符
using namespace std;

typedef struct Producer//产生式数据结构
{
	int left;
	vector<int> right;
}Producer;

unordered_map<string, int> ntsymTable;//非终结符表 映射至数字编码
unordered_map<string, int> tsymTable;//终结符表 映射至数字编码
unordered_map<int, int> terminSet;//存储终结符,并且要在其中加入结束符号#
unordered_map<int, int> nonterminSet;//存储非终结符,并同时存储一个到action表头的对应关系
unordered_map<int, string> symclaTable;//存储语义值类型的表

unordered_set<int> leftTable;//左结合表
unordered_set<int> rightTable;//右结合表
unordered_map<int, int> PriorityTable;//优先级表，前一项编码后一项优先级

vector<Producer> producerVector;//向量中存储了所有的产生式
vector<int> producerPreVector;//向量中储存了所有的产生式优先级
vector<string> producerActionVector;//向量中存储了所有产生式的动作
unordered_map<int, vector<int> > producerMap;//以产生式的左部为关键字，以对应产生式的编号为内容的哈希表

vector<vector<int>> actionTable;//二维向量，项目集转换动作表

int ExplanationProcess();//读取文件并分析说明段
int SymbolProcess();//读取文件并分析各符号
int RuleProcess();//读取文件并分析规则段
int ProducerProcess();//处理规则段之中的生成式
void AdditionalProgramProcess();//处理附加程序段
void GenerateCode();//代码生成

void pushtoTsymTable(string);//加入非终结符表
void pushtoNtsymTable(string);//加入终结符表
void GenerateProducerMap();



class LR1Item//LR1项目类
{
public:
	LR1Item(int np, int pos);//构造函数
	void move();
	int getCurrentSymbol() const;//返回下一个要移进的符号，为空则返回-1
	int getcurrentPosition() const;
	int getNextSymbol() const; 
	int getProducerNum() const;
	bool isEnd() const;//是否右部已经全部移进。
	bool nextIsEnd() const;//右部是否到达最后一个符号，求闭包运算时用。
	void setSearchSym(const unordered_set<int>& searchSymbol1);
	const unordered_set<int>& getSearchSym() const;
	bool checkEqual(const LR1Item& it1) const;//检查两个项目是否相等
	bool checkLike(const LR1Item& it1) const;//检查两个项目是否只有预测符不等
	size_t itemHasher() const;
	friend bool operator <(const LR1Item& it1, const LR1Item& it2);
private:
	int producerNum;//产生式的编号
	int currentPos;//该项目目前移进位置
	unordered_set<int> searchSymbol;//项目集的前向预测符；
};
LR1Item::LR1Item(int np, int pos) {
	producerNum = np;
	currentPos = pos;
}
void LR1Item::move()
{
	currentPos++;
}
int LR1Item::getCurrentSymbol() const//返回下一个移进符号
{
	int num = producerVector[producerNum].right.size();
	if (currentPos >= num) return -1;
	int s = producerVector[producerNum].right[currentPos];
	return s;
}
int LR1Item::getcurrentPosition() const
{
	return currentPos;
}
int LR1Item::getNextSymbol() const
{
	int num = producerVector[producerNum].right.size();
	if (currentPos + 1 >= num) return -1;
	int s = producerVector[producerNum].right[currentPos + 1];
	return s;
}
int LR1Item::getProducerNum() const
{
	return producerNum;
}
bool LR1Item::isEnd() const
{
	if (producerVector[producerNum].right[0] == EPSILON) return 1;
	int num = producerVector[producerNum].right.size();
	if (currentPos == num) return 1;
	else return 0;
}
bool LR1Item::nextIsEnd() const
{
	int num = producerVector[producerNum].right.size();
	if (currentPos + 1 == num) return 1;
	else return 0;
}
void LR1Item::setSearchSym(const unordered_set<int>& searchSymbol1)
{
	for (auto& i : searchSymbol1)
	{
		searchSymbol.insert(i);
	}
}
const unordered_set<int>& LR1Item::getSearchSym() const
{
	return searchSymbol;
}
bool LR1Item::checkEqual(const LR1Item& it1) const
{
	//检查两个item是否完全相同
	vector<bool> check;
	check.clear();
	if (it1.producerNum != this->producerNum || it1.currentPos != this->currentPos)
		return false;
	if (it1.getSearchSym().size() != this->getSearchSym().size())
		return false;
	unordered_set<int> tmpsym1 = it1.getSearchSym();
	unordered_set<int> tmpsym2 = this->getSearchSym();
	for (auto& s1 :tmpsym2)
	{
		tmpsym1.insert(s1);
	}
	if (tmpsym1.size() == tmpsym2.size()) return true;
	else return false;
}
bool LR1Item::checkLike(const LR1Item& it1) const
{
	vector<bool> check;
	check.clear();
	if (it1.producerNum != this->producerNum || it1.currentPos != this->currentPos)
		return false;
	else
	{
		unordered_set<int>v1 = it1.getSearchSym();
		unordered_set<int>v2 = this->getSearchSym();
		if (v1.size() != v2.size()) return true;
		else
		{
			for (auto& sym : v1)
			{
				v2.insert(sym);
			}
			//v2变为v1、v2交集
			if (v1.size() != v2.size()) return true;
			else return false;
		}
	}
	return false;
}

size_t LR1Item::itemHasher() const
{
	int pn = this->getProducerNum(); 
	int cp = this->getcurrentPosition();
	size_t hashResult = (((size_t)pn * 1000) + cp);
	for (auto& i : this->getSearchSym())
	{
		hashResult = (hashResult  + i)% 20000753;
	}
	return hashResult;
}

bool operator<(const LR1Item& it1, const LR1Item& it2)
{
	if (it1.producerNum == it2.producerNum)
	{
		//if (it1.currentPos == it2.currentPos)
		//	return it1.getSearchSym() < it2.getSearchSym();
		/*else*/ return it1.currentPos < it2.currentPos;
	}
	else return it1.producerNum < it2.producerNum;
}


void closure(multiset<LR1Item>& itemset);//LR（1）项目集闭包
void first(const LR1Item& item, unordered_set<int>& searchSymbol);
bool getfirstSet(int producerNum, unordered_set<int>& firstSet, unordered_set<int>& isUsed);
int ProcessItemSet();//LR(1)项目集转换关系构造，构造项目集族
void PrintItemSet(multiset<LR1Item>&i0);//输出项目集到屏幕上
void PrintItem(LR1Item&item);//输出单个LR(1)项目到屏幕上
void PrintSymbol();//输出终结符、非终结符到屏幕上
void PrintActionTable();
size_t hashItemSet(multiset<LR1Item> s1);
bool checkItemSet(multiset<LR1Item> s1, multiset<LR1Item>s2);//检查两个项目集是不是一致
multiset<LR1Item> minimizeItemset(multiset<LR1Item> s1);//将项目集中只有预测符不同的项目合并


string getSymbol(int);//从编号倒推回符号

ifstream fin;//输入文法
ofstream fout;//输出语法分析器
ofstream hout;

int main()
{
	string inname;
	cout << "输入文件名：";
	cin >> inname;
	fin.open(inname);
	fout.open("yyparse.cpp");
	hout.open("yytab.h");//生成全局符号语义值变量的类型	

	if (fin.fail() || fout.fail()) cout << "打开文件失败。\n";
	int error;
	error=ExplanationProcess();
	if (error == 0) cout << "语法规则说明段有错误,或是无说明段\n";
	error = SymbolProcess();
	if (error == 0) cout << "符号定义有错误\n";
	error = RuleProcess();
	if (error == 0) cout << "规则段有错误\n";
	AdditionalProgramProcess();
	cout << inname << "文件读取已经结束\n";
	PrintSymbol();
	//开始构造项目集并从项目集中构造分析表
	error = ProcessItemSet();
	if (error == 0) cout << "项目集转换关系生成出错\n";

	PrintActionTable();
	Beep(1200, 100);
	GenerateCode();
}

int ExplanationProcess()
{
	char a = fin.get();
	char b = fin.get();
	if (a != '%'||b!='{') {
		fin.unget();
		fin.unget();
		return 0;
	}
	string s;
	getline(fin, s);
	while (s[0] != '%' || s[1] != '}') {
		if (s != "") 		fout << s << endl;
		getline(fin, s);
	}//说明部分直接输出到文件
	cout << "已经将开始部分的程序输出到文件.\n";
	return 1;
	
}
int SymbolProcess()
{
	string line=" ";
	while (line!="%%")
	{
		fin >> line;
		//token union type left right 分别考虑
		if (line[1]=='t'&&line[2]=='o')//token开头  token是终结符
		{
			getline(fin, line);
			int lpos = 0;
			int pos = 0;
			string t;
			string typeoft;
			lpos = line.find("<", pos);
			pos = line.find(">", pos);
			if (lpos == line.npos)
			{
				typeoft = "";
				lpos = 0;
				pos = 0;
			}
			//如果没有规定语义值类型则置为空
			else
			{
				typeoft = line.substr(lpos + 1, pos - lpos - 1);
			}
			//cout << typeoft<<"!!!!\n";

			while (1) {
				//cout << "test";
				lpos = line.find(" ", pos);
				pos = line.find(" ", lpos + 1);
				//cout << lpos << " " << pos;
				if (lpos == line.npos ) break;
				t = line.substr(lpos + 1, pos - lpos - 1);

				pair<string, int> p1;
				pair<int, int>p2;
				p1.first = t;
				//cout << "t " << t << endl;
				p1.second = TERMINS + tsymTable.size();
				tsymTable.insert(p1);
				p2.first = p1.second;
				p2.second = terminSet.size() + nonterminSet.size();
				terminSet.insert(p2);

				pair<int, string> p3;
				p3.first = p1.second;
				p3.second = typeoft;
				symclaTable.insert(p3);
			}
			continue;
		}
		if (line[1] == 't' && line[2] == 'y')//type开头 为非终结符
		{
			getline(fin, line);
			int lpos = 0;
			int pos = 0;
			string t;
			string typeoft;
			lpos = line.find("<", pos);
			pos = line.find(">", pos);
			if (lpos == line.npos)
			{
				typeoft = "";
			}
			//如果没有规定语义值类型则置为空
			else
			{
				typeoft = line.substr(lpos + 1, pos - lpos - 1);
			}
			while (1) {
				lpos = line.find(" ", pos);
				pos = line.find(" ", lpos + 1);
				if (lpos == line.npos) break;
				t = line.substr(lpos + 1, pos - lpos - 1);
				//t为截断出来的type
				pair<string, int> p1;
				pair<int, int>p2;
				p1.first = t;
				p1.second = NONTERMINS + ntsymTable.size();
				ntsymTable.insert(p1);
				p2.first = p1.second;
				p2.second = terminSet.size() + nonterminSet.size();
				nonterminSet.insert(p2);

				pair<int, string> p3;
				p3.first = p1.second;
				p3.second = typeoft;
				symclaTable.insert(p3);
			}
			continue;
		}
		if (line == "%left" || line == "%right")
		{
			string state = line;
			static int priority = 0;//优先级，往下一行就加一
			priority++;
			//操作符也是终结符
			getline(fin, line);
			int lpos = 0;
			int pos = 0;
			string t;
			while (1) {
				lpos = line.find("'", pos);
				pos = line.find("'", lpos + 1);
				if (lpos == line.npos) break;
				t = line.substr(lpos + 1, pos - lpos - 1);
				//t为截断出来的type
				pair <string, int> p1;
				pair <int, int> p2;
				if (t != ""&&t!=" ") {
					pair<string, int> p1;
					pair<int, int>p2;
					p1.first = t;
					p1.second = TERMINS + tsymTable.size();
					tsymTable.insert(p1);
					p2.first = p1.second;
					p2.second = terminSet.size() + nonterminSet.size();
					terminSet.insert(p2);
					//放进终结符表中
					//接下来往左右结合表里加
					if (state == "%left")
					{
						leftTable.insert(p1.second);
					}
					if (state == "%right")
					{
						rightTable.insert(p1.second);
					}
					pair<int, int>p3;
					p3.first = p1.second;
					p3.second = priority;
					PriorityTable.insert(p3);
				}

			}
			continue;
		}
		//已处理完毕部分终结符表和非终结符表
		if (line == "%union")
		{
			char c = fin.get();
			while (c != '{' && c != -1) c = fin.get();
			if (c == -1) return 0;
			hout << "#ifndef YYVAL_H\n";//输出到头文件
			hout << "#define YYVAL_H\n";
			fout << "union SV\n";
			hout << "union SV\n";
			fout << "{\n";
			hout << "{\n";
			while ((c = fin.get()) != '}') { fout.put(c); hout.put(c); }
			fout << "};\n";
			hout << "};\n";
			fout << "SV u[10];\n";//在此声明数目为10的联合数组，以便后面语义动作时调用。
			fout << "SV yyval;\n";//在此声明全局变量yyval，用于返回lex符号的语义值
			hout << "#endif";
			continue;
		}
	
		
	}

	cout << "说明部分分析完毕\n";

	fin.unget();
	fin.unget();
	return 1;
}
int RuleProcess()
{
	//读取规则段并处理
	char a = fin.get();
	char b = fin.get();
	if (a != '%' || b != '%')
	{
		fin.unget();
		fin.unget();
		return 0;
	}

	Producer p0;
	p0.left = 0;
	p0.right.clear();
	producerActionVector.push_back("");
	producerVector.push_back(p0);
	producerPreVector.push_back(0);
	//预先加入一个产生式，日后在其中放S'->S，以便产生拓广文法

	ProducerProcess();
	a = ' ';
	while (1)//碰到%之前一直往下读
	{
		fin.unget();
		while (a == ' ' || a == '\n' || a == '\t' || a == '\r' || a == -1)
		{
			a = fin.get();
		}
		a = fin.get();
		int e=ProducerProcess();
		if (e == 0) break;
	}
	cout << "共" << producerVector.size() << "个产生式\n";
	producerVector[0].left = SSTART;//拓广文法开始符号S'标记为39999
	producerVector[0].right.push_back(producerVector[1].left);

	pair<string, int> p1_pound;
	string s1 = "#";
	p1_pound.first = s1;
	p1_pound.second = POUND;
	tsymTable.insert(p1_pound);
	pair<int, int> p2_pound;
	p2_pound.first = p1_pound.second;
	p2_pound.second= terminSet.size() + nonterminSet.size();
	terminSet.insert(p2_pound);
	//将#号加入终结符表
	GenerateProducerMap();


	return 1;
}
int ProducerProcess()
{
	string line;
	char c = fin.get();
	c = fin.get();
	while (c != ' ' && c != '\n' && c != '\t' && c != '\r' && c != -1)
	{
		line.push_back(c);
		c = fin.get();
		//把产生式第一个词读进来
		//它可能不在非终结符表中，要补充进去
	}
	//cout << "line:" << line << endl;
	if (line[0] == '%') {
		cout << "产生式读取已经结束" << endl;
		return 0;
	}
	if (tsymTable.count(line))
	{
		cout << "定义错误 非终结符" << line << "出现在了终结符表中" << endl;
		return 0;
	}
	if (!ntsymTable.count(line))
	{
		//不在非终结符表中，要填进去
		pushtoNtsymTable(line);

		//完成，已经加入非终结符表中
	}
	string pleft = line;//产生式左部符号暂存
	c = fin.get();
	while (c == ' ' || c == '\n' || c == '\r' || c == '\t') c = fin.get();
	if (c != ':')
	{
		cout << "产生式格式错误，没有冒号!" << endl;
		return 0;
	}
	while (1)
	{
		Producer p1;
		p1.left = ntsymTable[pleft];
		//产生式左侧符号的数字编码放进产生式
		int priority = 0;
		//cout << "test";
		while (1)
		{
			//cout << "test";
			c = fin.get();
			while (c == ' ' || c == '\n' || c == '\r' || c == '\t') c = fin.get();
			//跳过空格
			if (c == ';' || c == '{' || c == '|') break;
			//产生式结束了，或者开始记录动作
			if (c == '\'')
			{
				c = fin.get();
				string op1 = "";//记录碰到的这个操作符
				while (c != '\'')
				{
					//cout << "test";
					op1.push_back(c);
					c = fin.get();
				}
				//cout << "读取到操作符" << op1 << endl;
				if (!tsymTable.count(op1))
				{
					//如终结符表里没有再往里加
					pair<string, int> p1;
					pair<int, int>p2;
					p1.first = op1;
					p1.second = TERMINS + tsymTable.size();
					tsymTable.insert(p1);
					p2.first = p1.second;
					p2.second = terminSet.size() + nonterminSet.size();;
					terminSet.insert(p2);

					pair<int, int> p3;
					p3.first = p1.second;
					p3.second = 0;
					//在产生式里第一次出现的操作符，默认优先级最低
				}
				p1.right.push_back(tsymTable[op1]);
				priority = PriorityTable[tsymTable[op1]];
				continue;
			}
			//如果读到了单引号’,说明碰到了操作符
			if (c == '\\')//用\e表示空,即ε
			{
				c = fin.get();
				if (c == 'e')
				{
					//产生式右边是空的，加入EMPTY
					p1.right.push_back(EPSILON);
					break;
				}
				else
				{
					cout << c << "符号错误" << endl;
					return false;
				}
			}
			else
			{
				string s1;
				while (c != ' ' && c != -1 && c != '\n' && c != '\t' && c != '\r')
				{
					s1.push_back(c);
					c = fin.get();

				}
				//cout << "读取到符号" << s1 << endl;
				if (tsymTable.count(s1)) p1.right.push_back(tsymTable[s1]);
				else
				{
					if (!ntsymTable.count(s1))
					{
						pushtoNtsymTable(s1);
					}
					p1.right.push_back(ntsymTable[s1]);
				}

			}
		}
		//以上是对单行产生式的处理
		//这时候已经将会读到{**}，这中间是语义动作
		//注意：{ }中间可能会有更多的{ }，以“}”进行结尾识别会出现错误！！
		string action = "";//语义动作
		if (c == '{')
		{
			int numofleft = 1;
			int numofright = 0;
			c = fin.get();
			while (c != '}' || numofleft != numofright)
			{
				if (c == '$')
				{
					c = fin.get();
					if (c == '$')
					{
						action.append("u[0]");
						//如果没有对应的语义值类型则不加
						if (symclaTable[p1.left] != "")
						{
							action.push_back('.');
							action.append(symclaTable[p1.left]);
						}
					}
					else
						if (c >= '1' && c <= '9')
						{
							int num = 0;
							action.append("u[");
							num += (c - '0');
							action.push_back(c);
							c = fin.get();
							while (c > '0' && c < '9')
							{
								//不止一位数
								num = num * 10 + (c - '0');
								action.push_back(c);
								c = fin.get();
							}
							fin.unget();
							action.push_back(']');
							string s;
							s = symclaTable[p1.right[num - 1]];
							if (s != "")
							{
								action.push_back('.');
								action.append(s);
							}
						}
						else
						{
							cout << "语义动作定义有错误" << endl;
							return 0;
						}
				}
				else action.push_back(c);
				c = fin.get();
				if (c == '}') numofright++;
				if (c == '{')numofleft++;
			}
			c = fin.get();
			while (c == ' ' || c == '\n' || c == '\r' || c == '\t') c = fin.get();

		}
		if (c == ';' || c == '|')
		{
			producerActionVector.push_back(action);
			cout << "读取到产生式为" << getSymbol(p1.left);
			cout << "->";
			for (int i=0;i<p1.right.size();i++)
			{
				cout << getSymbol(p1.right[i]) << " ";
			}
			cout<< " \n动作为";
			if (action == "") cout << "空\n";
			else cout << action << endl;
			cout << "\n--------------------------------------------------\n";
			producerVector.push_back(p1);
			producerPreVector.push_back(priority);
			if (c == ';') break;

		}
		else
		{
			cout << c << " 产生式结尾没有分号，格式错误" << endl;
			return 0;
		}

	}
	return 1;
}
void AdditionalProgramProcess()
{
	string line;
	//getline(fin,line);
	while (getline(fin, line))
	{
		if (line != "") fout << line << endl;
	}
}
void GenerateCode()
{
	fout << "#include<iostream>\n";
	fout << "#include<unordered_map>\n";
	fout << "#include<stack>\n";
	fout << "#include<fstream>\n";
	fout << "#include<string>\n";
	fout << "#define ERROR 40000\n";
	fout << "#define ACCEPT 0\n";
	fout << "#define SOURCE_END 40001\n";
	fout << "using namespace std;\n";
	fout << "extern int analysis(char *yytext,int n);\n";
	fout << "struct Sym\n";
	fout << "{\n";
	fout << "\tint symbol;\n";
	fout << "\tint state;\n";
	fout << "};\n";
	fout << "unordered_map<int,int> signalTable;\n";
	fout << "unordered_map<String,int> symTable;\n";
	fout << "int producerN[" << producerVector.size() << "]={";
	for (int i = 0; i < producerVector.size(); i++)
	{
		fout << producerVector[i].right.size();
		if (i != producerVector.size() - 1)
			fout << ",";
	}
	fout << "};\n";
	fout << "int pLeftSection[" << producerVector.size() << "]={";
	for (int i = 0; i < producerVector.size(); i++)
	{
		fout << producerVector[i].left;
		if (i != producerVector.size() - 1)
			fout << ",";
	}
	fout << "};\n";
	fout << "ifstream fin;\n";
	//输出文件定义的一些常量
	fout << "int actionTable[" << actionTable.size() << "][" << actionTable.front().size() << "]=\n\t\t{";
	for (int i = 0; i < actionTable.size(); i++)
	{
		fout << "\t\t{";
		for (int j = 0; j < actionTable.front().size(); j++)
		{
			if (actionTable[i][j] != 40000)
				fout << actionTable[i][j];
			else
				fout << "ERROR";
			if (j == actionTable.front().size() - 1) continue;
			fout << ",";
		}
		fout << "}";
		if (i == actionTable.size() - 1) continue;
		fout << ",\n";
	}
	fout << "};\n";
	fout << "int searchTable(int cstate,char symbol)\n";
	fout << "{\n";
	fout << "\treturn actionTable[cstate][symbol];\n";
	fout << "}\n";
	fout << "\n";
	//此部分完成查表程序的输出
	//下面完成读Token程序的输出
	fout << "int readToken()\n";
	fout << "{\n";
	fout << "\tif(fin.eof())\n";
	fout << "\t\treturn SOURCE_END;\n";
	fout << "\tchar buf[256];//不支持超过256个字符的符号\n";
	fout << "\tint pos=0;\n";
	fout << "\tbool isEnd=false;\n";
	fout << "\tbool isToken=false;//与isOperator互斥\n";
	fout << "\tbool isOperator=false;\n";
	fout << "\tstring ops(\"!@#$%^&*()+-=|\[]{};':\\\",.<>/?\");\n";
	fout << "\tstring wss(\"\\t\\n\\r \");\n";
	fout << "\tstring letter(\"_0123456789aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ\");\n";

	fout << "\tchar c=fin.get();\n";
	fout << "\twhile(c==' '||c=='\\t'||c=='\\n'||c=='\\r') c=fin.get();//滤掉空白符\n";
	fout << "\twhile(!isEnd)\n";
	fout << "\t{\n";
	fout << "\t\tif(c==-1)\n";
	fout << "\t\t{\n";
	fout << "\t\t\tbuf[pos]='\\0';\n";
	fout << "\t\t\tisEnd=true;\n";
	fout << "\t\t\tcontinue;\n";
	fout << "\t\t}";
	fout << "\t\tif(ops.find(c)>=0&&ops.find(c)<ops.size())//表示当c不是正常字符时。\n";
	fout << "\t\t{\n";
	fout << "\t\t\t//进入非正常字符的处理过程\n";
	fout << "\t\t\tif(isToken)\n";
	fout << "\t\t\t{\n";
	fout << "\t\t\t\tfin.unget();\n";
	fout << "\t\t\t\tbuf[pos]='\\0';\n";
	fout << "\t\t\t\tisEnd=true;\n";
	fout << "\t\t\t}\n";
	fout << "\t\t\telse\n";
	fout << "\t\t\t{\n";
	fout << "\t\t\t\tisOperator=true;\n";
	fout << "\t\t\t\tbuf[pos]=c;\n";
	fout << "\t\t\t\tpos++;\n";
	fout << "\t\t\t\tc=fin.get();\n";
	fout << "\t\t\t}\n";
	fout << "\t\t\tcontinue;\n";
	fout << "\t\t}\n";
	fout << "\t\tif(letter.find(c)>=0&&letter.find(c)<letter.size())\n";
	fout << "\t\t{\n";
	fout << "\t\t\tif(isOperator)\n";
	fout << "\t\t\t{\n";
	fout << "\t\t\t\tfin.unget();\n";
	fout << "\t\t\t\tbuf[pos]='\\0';\n";
	fout << "\t\t\t\tisEnd=true;\n";
	fout << "\t\t\t}\n";
	fout << "\t\t\telse\n";
	fout << "\t\t\t{\n";
	fout << "\t\t\t\tisToken=true;\n";
	fout << "\t\t\t\tbuf[pos]=c;\n";
	fout << "\t\t\t\tpos++;\n";
	fout << "\t\t\t\tc=fin.get();\n";
	fout << "\t\t\t}\n";
	fout << "\t\t\tcontinue;\n";
	fout << "\t\t}\n";
	fout << "\t\tif(wss.find(c)>=0&&wss.find(c)<wss.size())\n";
	fout << "\t\t{\n";
	fout << "\t\t\tbuf[pos]='\\0';\n";
	fout << "\t\t\tisEnd=true;\n";
	fout << "\t\t}\n";
	fout << "\t\t\n";
	fout << "\t\t\n";

	fout << "\t\telse return -1;\n";
	fout << "\t}\n";
	fout << "\treturn analysis(buf,strlen(buf));\n";
	fout << "}\n";
	//接下来输出语义动作执行函数
	fout << "void runaction(int num)\n";
	fout << "{\n";
	fout << "\tswitch(num)\n";
	fout << "\t{\n";
	for (int i = 0; i < producerActionVector.size(); i++)
	{
		if (producerActionVector[i].size() != 0)
		{
			fout << "\tcase " << i + 1 << ":\n";
			fout << "\t\t{\n";
			fout << "\t\t\t" << producerActionVector[i] << "\n";
			fout << "\t\t\tbreak;\n";
			fout << "\t\t}\n";
		}
	}
	fout << "\t}\n";
	fout << "}\n";
	//
	fout << "void getvalue(int symbol,SV & val)\n";
	fout << "{\n";
	fout << "\tswitch(symbol)\n";
	fout << "\t{\n";
	for (unordered_map<int, int>::iterator pi = terminSet.begin(); pi != terminSet.end(); pi++)
	{
		if (symclaTable.count(pi->first))
			fout << "\tcase " << pi->first << ":val." << symclaTable[pi->first] << "=" << pi->first
			<< ";break;\n";
	}
	fout << "\t}\n";
	fout << "}\n";
	//
	fout << "int parse()\n";
	fout << "{\n";
	fout << "\tint inputsymbol=0;\n";
	fout << "\tint cstate=0;\n";
	fout << "\tstack<Sym> symStack;//符号栈\n";
	fout << "\tstack<SV> valStack;//语义值栈\n";
	fout << "\tSym st;//用作分析时的临时栈顶元素存储变量\n";
	fout << "\tst.symbol=0;\n";
	fout << "\tst.state=0;\n";
	fout << "\tSV val;\n";
	fout << "\tsymStack.push(st);//语义值栈必须要和符号栈同步\n";
	fout << "\tvalStack.push(val);\n";
	fout << "\tinputsymbol=readToken();\n";
	fout << "\twhile(1)\n";
	fout << "\t{\n";
	fout << "\t\tst=symStack.top();\n";
	fout << "\t\tint col=signalTable[inputsymbol];\n";
	fout << "\t\tint result=searchTable(st.state,col);\n";
	fout << "\t\tif(result==E)//出错\n";
	fout << "\t\t{\n";
	fout << "\t\t\tcout<<\"Compile Error!\"<<endl;\n";
	fout << "\t\t\treturn 0;\n";
	fout << "\t\t}\n";
	fout << "\t\tif(result==ACCEPT)\n";
	fout << "\t\t{\n";
	fout << "\t\t\tcout<<\"Compile sucessfully!\"<<endl;\n";
	fout << "\t\t\treturn 1;\n";
	fout << "\t\t}\n";
	fout << "\t\tif(result<0)//负数表示为归约项目\n";
	fout << "\t\t{\n";
	fout << "\t\t\tresult*=-1;\n";
	fout << "\t\t\tint n=producerN[result];//取得该号产生式右部符号数量，以作弹栈用\n";
	fout << "\t\t\tfor(int i=0;i<n;i++)\n";
	fout << "\t\t\t{\n";
	fout << "\t\t\t\tsymStack.pop();\n";
	fout << "\t\t\t\tu[n-i]=valStack.top();\n";
	fout << "\t\t\t\tvalStack.pop();\n";
	fout << "\t\t\t}\n";
	fout << "\t\t\trunaction(result);//执行语义动作\n";
	fout << "\t\t\t//再将产生式左部的符号压栈，语义值一同压栈\n";
	fout << "\t\t\tst.symbol=pLeftSection[result];\n";
	fout << "\t\t\tst.state=searchTable(symStack.top().state,signalTable[st.symbol]);\n";
	fout << "\t\t\tsymStack.push(st);\n";
	fout << "\t\t\t//将产生式左部符号的语义值入栈。这个值在runaction()中已经修改\n";
	fout << "\t\t\tvalStack.push(u[0]);\n";
	fout << "\t\t}\n";
	fout << "\t\telse\n";
	fout << "\t\t{\n";
	fout << "\t\t\tst.symbol=inputsymbol;\n";
	fout << "\t\t\tst.state=result;\n";
	fout << "\t\t\tsymStack.push(st);\n";
	fout << "\t\t\tSV tu;\n";
	fout << "\t\t\tif(yyval.ival==0)\n";
	fout << "\t\t\t\tgetvalue(inputsymbol,tu);\n";
	fout << "\t\t\telse tu=yyval;\n";
	fout << "\t\t\tvalStack.push(tu);\n";
	fout << "\t\t\tinputsymbol=readToken();\n";
	fout << "\t\t}\n";
	fout << "\t}\n";
	fout << "}\n";
	//输出main函数
	fout << "void main()\n";
	fout << "{\n";
	/*
	for(unordered_map<string,int>::iterator pti=tsymTable.begin();pti!=tsymTable.end();pti++)
	{
		if(pti->first!="#")
		fout<<"\tconst int "<<pti->first<<"="<<pti->second<<";\n";//定义各变量，变量名用
	}*/
	fout << "\tpair<string,int> p1;\n";//输出符号名和对应编号
	for (auto& i : tsymTable)
	{
		if (i.first != "#")
		{
			fout << "\tp1.first=\"" << i.first << "\";\n";
			fout << "\tp1.second=" << i.second << ";\n";
			fout << "\tsymTable.insert(p1);\n";
		}
		else
		{
			fout << "\tp1.first=\"#\";\n";
			fout << "\tp1.second=" << i.second << ";\n";
			fout << "\tsymTable.insert(p1);\n";
		}
	}
	for (auto& i : ntsymTable)
	{
		fout << "\tp1.first=\"" << i.first << "\";\n";
		fout << "\tp1.second=" << i.second << ";\n";
		fout << "\tsymTable.insert(p1);\n";
	}
	fout << "\tpair<int,int> tp;\n";
	for (unordered_map<string, int>::iterator pti = tsymTable.begin(); pti != tsymTable.end(); pti++)
	{
		if (pti->first != "#")
		{
			fout << "\ttp.first=" << pti->second << ";\n";
			fout << "\ttp.second=" << terminSet[pti->second] << ";\n";
			fout << "\tsignalTable.insert(tp);\n";
		}
		else
		{
			fout << "\ttp.first=40001;\n";
			fout << "\ttp.second=" << terminSet[pti->second] << ";\n";
			fout << "\tsignalTable.insert(tp);\n";
		}
	}
	for (unordered_map<int, int>::iterator pti = nonterminSet.begin(); pti != nonterminSet.end(); pti++)
	{
		fout << "\ttp.first=" << pti->first << ";\n";
		fout << "\ttp.second=" << pti->second << ";\n";
		fout << "\tsignalTable.insert(tp);\n";
	}
	//以上完成signalTable的初始化段代码.
	fout << "\tstring filename;\n";
	fout << "\tcout<<\"Please input the file name:\"<<endl;\n";
	fout << "\tcin>>filename;\n";
	fout << "\tfin.open(filename.c_str());\n";
	fout << "\tif(fin.fail())\n";
	fout << "\t{\n";
	fout << "\t\tcout<<\"Cannot open the file \"<<filename<<endl;\n";
	fout << "\t\treturn;\n";
	fout << "\t}\n";
	fout << "\tparse();\n";
	fout << "}\n";
}
void pushtoTsymTable(string s)
{
	pair<string, int> p1;
	pair<int, int>p2;
	p1.first = s;
	p1.second = TERMINS + tsymTable.size();
	tsymTable.insert(p1);
	p2.first = p1.second;
	p2.second= terminSet.size() + nonterminSet.size();
	terminSet.insert(p2);
}
void pushtoNtsymTable(string s)
{
	pair<string, int> p1;
	pair<int, int>p2;
	p1.first = s;
	p1.second =NONTERMINS + ntsymTable.size();
	ntsymTable.insert(p1);
	p2.first = p1.second;
	p2.second = terminSet.size() + nonterminSet.size();
	nonterminSet.insert(p2);
}
void GenerateProducerMap()
{
	for (int i = 1; i < producerVector.size(); i++)
	{//0号产生式为S'->S,此处不考虑
		if (!producerMap.count(producerVector[i].left))
		{
			pair<int, vector<int>> p1;
			p1.first = producerVector[i].left;//产生式的左部
			vector<int> v1;
			v1.clear();
			p1.second = v1;
			producerMap.insert(p1);
		}
		producerMap[producerVector[i].left].push_back(i);
	}
}
bool checkItemSet(multiset<LR1Item> s1, multiset<LR1Item> s2)
{
	//每个项目的currpos、producernum、预测符一致
	

	multiset <LR1Item> tmp1 = s1;
	multiset <LR1Item> tmp2 = s2;
	vector<bool> checkset;
	checkset.clear();
	if (s1.size() != s2.size()) return false;

	//首先用hash函数判断，如果不一致直接返回false;
	size_t r1 = hashItemSet(s1);
	size_t r2 = hashItemSet(s2);
	if (r1 != r2) return false;

	for (auto& item1 : s1)
	{
		bool check = 0;
		for (auto& item2 : s2)
		{
			if (item1.checkEqual(item2))
			{
				checkset.push_back(true);
				check = 1;
				break;
			}
		}
		if (check == 0) return false;
	}
	return true;
	//if (checkset.size() != s1.size()) return false;
	//else return true;
}
multiset<LR1Item> minimizeItemset(multiset<LR1Item> s1)
{
	multiset<LR1Item> tmpset;
	tmpset.clear();
	for (auto& itm1 : s1)
	{
		unordered_set<int> searchsymSum;
		searchsymSum.clear();
		multiset<LR1Item>::iterator pos;
		LR1Item tmpitem(0,0);
		bool check = 0;
		for (auto& itm2 : tmpset)
		{
			if (itm1.checkLike(itm2))
			{
				check = 1;
				//找到相似项目，开始合并
				searchsymSum = itm1.getSearchSym();
				for (auto& sym : itm2.getSearchSym())
				{
					searchsymSum.insert(sym);
				}
				pos=tmpset.find(itm2);
				break;
			}
		}
		if (check == 0) tmpset.insert(itm1);
		else
		{
			LR1Item itm3 = *pos;
			tmpset.erase(pos);
			itm3.setSearchSym(searchsymSum);
			tmpset.insert(itm3);
		}
	}
	return tmpset;
}
multiset<LR1Item> mergeItemSet(multiset<LR1Item> s1, multiset<LR1Item> s2)
{
	vector<LR1Item> v1;
	for (auto& item : s1)
	{
		v1.push_back(item);
	}
	for (int i = 0; i < v1.size(); i++)
	{
		for (auto& j : s2)
		{
			if (v1[i].checkEqual(j))
			{
				//找到相等的二个项目，开始合并
				unordered_set<int> symset1 = v1[i].getSearchSym();
				unordered_set<int> symset2 = j.getSearchSym();
				for (auto& x : symset2)
				{
					symset1.insert(x);
				}
				v1[i].setSearchSym(symset1);
			}
		}
	}
	multiset<LR1Item> result;
	result.clear();
	for (int i = 0; i < v1.size(); i++)
	{
		result.insert(v1[i]);
	}
	return result;
}
void closure(multiset<LR1Item>& itemSet)//itemset即一个项目集
{
	queue<LR1Item> q;//队列中为项目集所有状态（除去已全部移进）
	for (auto &ps : itemSet)
	{
		if (!ps.isEnd() && nonterminSet.count(ps.getCurrentSymbol()))//
			q.push(ps);
	}
	//unordered_set<int> isU;
	//unordered_set<int> isE;
	while (!q.empty())
	{
		LR1Item citem = q.front();
		q.pop();
		vector<int> vpi = producerMap[citem.getCurrentSymbol()];
		unordered_set<int> searchSym;
		//PrintItem(citem);
		
		//producerVector[citem.getProducerNum()].left
		first(citem, searchSym);//此步完成预测符的计算	
		//if (searchSym.size() == 0) cout << "\nWARNING";
		//else cout << "\nYES";
		
		for (int i = 0; i < vpi.size(); i++)
		{
			LR1Item tp(vpi[i], 0);
			//first(tp, searchSym);
			tp.setSearchSym(searchSym);
			multiset<LR1Item>::iterator iter = itemSet.begin();
			//!!!
			int flag1=0;

			for (auto & it1 : itemSet)
			{
				if (it1.checkEqual(tp)) {
					/*
					if (tp.getProducerNum() == 8)
					{
						PrintItem(tp);
						PrintItem(it1);
						cout << "两个项目相同\n";
					}
					*/
					flag1 = 1;
					break;
				}
			}
			/*
			if (tp.getProducerNum() == 8 && tp.getSearchSym().count(20001))
			{
				PrintItem(tp);
				if (flag1 == 1) cout << "\nwarning\n";
			}
			*/
			if (flag1==0)//如果此项目不在项目集中，则需将它加入到项目集中去。
			{                //同时需要进行相应的预测符的计算。
				itemSet.insert(tp);
				//if (itemSet == s2) cout << "插入无效";
			
 				if (nonterminSet.count(tp.getCurrentSymbol()))
				{
					q.push(tp);
				}
			}
		}
	}
}
void first(const LR1Item& item, unordered_set<int>& searchSymbol)
{
	if (item.nextIsEnd()) searchSymbol = item.getSearchSym();
	else {
		int symbol = item.getNextSymbol();
		if (terminSet.count(symbol)) searchSymbol.insert(symbol);//如果是终结符就加入预测符
		else {
			vector<int> v1 = producerMap[symbol];
			//v1为所有开始符号为symbol的产生式
			unordered_set<int> isused;
			for (int i = 0; i < v1.size(); i++)
			{
				getfirstSet(v1[i], searchSymbol, isused);
			}
		}
	}
}
bool getfirstSet(int producerNum, unordered_set<int>& firstSet, unordered_set<int>& isUsed)
{
	if (producerVector[producerNum].right[0] == EPSILON) return 0;
	else {
		int i = 0;
		if (terminSet.count(producerVector[producerNum].right[i]))
			firstSet.insert(producerVector[producerNum].right[i]);
		else if (nonterminSet.count(producerVector[producerNum].right[i]))
		{
			if (!isUsed.count(producerVector[producerNum].right[i]))
			{
				isUsed.insert(producerVector[producerNum].right[i]);
				vector<int> v1;
				v1 = producerMap[producerVector[producerNum].right[i]];
				int c = 1;
				for (auto& p1 : v1)
				{
					if (!getfirstSet(p1, firstSet, isUsed)) c = 0;
				}
				while (c == 0)
				{
					i++;
					if (producerVector[producerNum].right[i] != 0 
						&& nonterminSet.count(producerVector[producerNum].right[i]) 
						&& isUsed.count(producerVector[producerNum].right[i]) == 0)
					{
						vector<int> v2;
						v2 = producerMap[producerVector[producerNum].right[i]];
						for (auto& p2 : v2)
						{
							if (!getfirstSet(p2, firstSet, isUsed)) c = 0;
						}
					}
					else if (terminSet.count(producerVector[producerNum].right[i]))
						firstSet.insert(producerVector[producerNum].right[i]);
				}
			}
		}
		return 1;
	}
}
int ProcessItemSet()
{
	struct ItemSetRow_Hash
	{
		multiset<LR1Item> IS;
		int r;//row
		int h;//hash
	};
	int itemState = 0;
	queue<multiset<LR1Item>> Q;//项目集队列
	vector<ItemSetRow_Hash> itemsetTable;
	//vector<pair<multiset<LR1Item>, int>> itemsetHashTable;
	//vector<pair<int,int>> tableHash;//存放table中每一个项目集的hash值 与row值

	//map有问题 需要改造
	unordered_map<int, multiset<LR1Item>> moveItemset;
	//以转换表中边的编号为关键词 后一项是项目集
	multiset<LR1Item> I0;//初始项目集
	LR1Item firstitem(0, 0);//编号0 移进位置0
	unordered_set<int> searchs;//预测符，放入#
	searchs.insert(POUND);
	firstitem.setSearchSym(searchs);
	I0.insert(firstitem);
	firstitem.move();
	closure(I0);

	ItemSetRow_Hash p1;
	multiset<LR1Item> I1 = minimizeItemset(I0);

	Q.push(I1);
	//cout << "输出初始项目集I0\n";
	//PrintItemSet(I0);

	//输出项目集中所有项目具体内容 （数量较多）
	
	p1.IS= I1;
	p1.r= itemState;
	p1.h = hashItemSet(I1);
	itemsetTable.push_back(p1);
	//p1.second = ;
	//itemsetHashTable.push_back(p1);

	int maxnum = nonterminSet.size() + terminSet.size();
	//一个项目集最多可以从这么多个边转到下一个项目集
	while (!Q.empty())
	{
		multiset<LR1Item> itemset = Q.front();
		//获取队列顶的一个项目集
		Q.pop();
		moveItemset.clear();

		vector<int> f(maxnum);
		//转换表的其中一行 长度固定为maxnum
		for (int i = 0; i < f.size(); i++)
			f[i] = ERROR;//空边用ERROR进行占位
		actionTable.push_back(f);

		int column, row;
		row = -1;
		int hh;
		//cout << itemsetTable.count(itemset);
		int h1 = hashItemSet(itemset);
		
		for (auto& p : itemsetTable)
		{
			if (p.h != hashItemSet(itemset))
				continue;
			if (checkItemSet(p.IS, itemset))
			{
				row = p.r;
				hh = p.h;
			}
		}
		
		//row = itemsetTable[itemset];
		if (row == -1)
		{
			cout << "项目集错误\n";
			return 0;
		}
			
		cout << "项目集("<<hh<<")状态号：" << row << endl;
		if (row <= 50)
		{
			PrintItemSet(itemset);
		}
		//if (row == 51)
		//	cout << "状态过多，停止输出" << endl;
		//Beep(1200, 100);
		for (auto& ps : itemset)
		{//项目集中的每一个项目分别move
			if (ps.isEnd())//以到达可归约
			{
				for (auto& pi : ps.getSearchSym())
				{
					column = terminSet[pi];
					if (actionTable[row][column] <= 0 && ps.getProducerNum() != actionTable[row][column] * (-1))
					{
						cout << "归约归约冲突！" << endl;
						cout << "row=" << row << " column=" << column << " old=" << actionTable[row][column]
							<< " new=" << ps.getProducerNum() << endl;
						return 0;
					}
					actionTable[row][column] = ps.getProducerNum() * (-1);
					//以负数代表归约项。0代表accept
					//cout << "actionTable row=" << row << " column=" << column << endl;
				}
			}
			else
			{
				LR1Item tp = ps;
				tp.move();
				moveItemset[ps.getCurrentSymbol()].insert(tp);
			}
		}
		//对新产生的项目集求闭包
		for (auto& ph : moveItemset)
		{
			//cout << "对以下项目集求闭包\n";
			//PrintItemSet(ph.second);
			
			closure(ph.second);
			//在这里应当加入减小项目集规模的函数
			// 将相同项目的预测符合并
			// 以便加快运行速度
			multiset<LR1Item> set1=minimizeItemset(ph.second);
			ph.second = set1;
			//cout << "碰到项目集\n";
			//PrintItemSet(ph.second);
			bool findset = 0;
			for (auto& p : itemsetTable)
			{
				if (checkItemSet(p.IS, ph.second))
					findset = 1;
			}
			/*
			if (findset == 0)cout << "加入\n状态号为" << itemState + 1 << endl;
			else cout << "不加入，与某一个相同\n";
			*/
			if (findset==0)
			{
				//findset = 0;
				ItemSetRow_Hash p2;
				p2.IS = ph.second;
				itemState++;
				p2.r = itemState;
				p2.h = hashItemSet(ph.second);
				itemsetTable.push_back(p2);
				//p2.second = hashItemSet(ph.second);
				//itemsetHashTable.push_back(p2);
				Q.push(ph.second);
			}
			//接下来生成转换表

			if (nonterminSet.count(ph.first))
				column = nonterminSet[ph.first];
			else
				column = terminSet[ph.first];
			int tmp = 0;
			for (auto& p : itemsetTable)
			{
				if (p.h != hashItemSet(ph.second))
					continue;
				if (checkItemSet(p.IS, ph.second))
					tmp = p.r;
			}
			if (actionTable[row][column] <= 0)
			{
				if (producerPreVector[actionTable[row][column] * (-1)] < PriorityTable[ph.first])
				{
					actionTable[row][column] = tmp;
					continue;
				}
				if (rightTable.count(ph.first))
				{
					actionTable[row][column] = tmp;
					continue;
				}
				if (leftTable.count(ph.first))//如果此操作符不在两个结合表中
				{
					continue;
				}
				continue;
				//如果上述条件都不满足，则报错。
				cout << "移进归约冲突" << endl;
				cout << "row=" << row << " column=" << column << " old=" << actionTable[row][column]
					<< " new=" << tmp
					<< " signal=" << ph.first << endl;
				return 0;
			}
			else
				actionTable[row][column] = tmp;
		}
	}
	return 1;
}
void PrintSymbol()
{
	cout << "输出读取的终结符表.\n";
	for (auto& k : tsymTable)
	{
		cout << k.first << " " << k.second << " " << symclaTable[k.second] << endl;
	}
	cout << "输出读取的非终结符表.\n";
	for (auto& k : ntsymTable)
	{
		cout << k.first << " " << k.second << " " << symclaTable[k.second] << endl;
	}
}
void PrintActionTable()
{
	//第一列
	if (actionTable[0].size() >= 50)
	{
		cout << "分析表格过大，停止输出\n";
		return;
	}
	cout << "\n";
	cout << setw(7) << " ";
	cout << "|" ;
	vector<int> symboltemp;
	for (int i = 0; i < actionTable[0].size(); i++)
	{
		for (auto& m : terminSet)
		{
			if (m.second == i) symboltemp.emplace_back(m.first);
		}

		for (auto& n : nonterminSet)
		{
			if (n.second == i) symboltemp.emplace_back(n.first);
		}
	}
	for (int i = 0; i < symboltemp.size(); i++)
	{
		cout <<setw(6)<< getSymbol( symboltemp[i])<<"|";
	}
	cout << "\n";
	for (int j = 0; j <= actionTable[0].size(); j++)
	{
		cout << "-------";
	}
	cout << "\n";
	for (int i = 0; i < actionTable.size(); i++)
	{
		cout << "state"<<setw(2) << i;
		cout << "|";
		for (int j = 0; j < actionTable[i].size(); j++)
		{
			int a = actionTable[i][j];
			int b = abs(a);
			string c = to_string(b);
			string rx = "r";
			string sx = "s";
			string gotox = "goto";
			rx.append(c);
			sx.append(c);
			gotox.append(c);
			if (a == ERROR) cout << setw(6) << " ";
			if (a == 0) cout << setw(6) << "accept";
			if (a < 0) cout << setw(6) << rx;
			if (a > 0 && a != ERROR)
			{
				cout << setw(6) << c;
			}
			cout << "|";
		}
		cout << "\n";
		cout << "-------";
		for (int j = 0; j < actionTable[i].size(); j++)
		{
			cout << "-------";
		}
		cout << "\n";
	}
}
size_t hashItemSet(multiset<LR1Item> s1)
{
	size_t result=0;
	for (auto& item : s1)
	{
		result += item.itemHasher();
		result %= 19260817;
	}
	return result;
}
string getSymbol(int t)
{
	/*
	#define TERMINS 20000 //终结符从20000以后开始编号
	#define NONTERMINS 30000 //非终结符从30000以后开始编号
	#define POUND 19999//#号标记为19999
	#define SSTART 29999//拓广文法开始符号S'标记为29999
	#define EPSILON 39999//产生式里可能会出现的空,平时标记为ε
	*/
	if (t == 19999) return"#";
	if (t == 29999) return "S'";
	if (t == 39999) return "null";
	if (t < 30000)
	{
		for (auto& p1 : tsymTable)
		{
			if (p1.second == t) return p1.first;
		}
	}
	else
	{
		for (auto& p1 : ntsymTable)
		{
			if (p1.second == t) return p1.first;
		}
	}
	return "";
}
void PrintItemSet(multiset<LR1Item>& i0)
{
	cout << "\n输出项目集：\n";
	for (auto& pi : i0)
	{
		cout << "项目号：" << pi.getProducerNum()<<" ";
		cout << getSymbol(producerVector[pi.getProducerNum()].left )<< "->";
		for (int i = 0; i < producerVector[pi.getProducerNum()].right.size(); i++)
		{
			if (pi.getcurrentPosition()==i) cout << "·";
			cout << getSymbol(producerVector[pi.getProducerNum()].right[i]) << " ";
		}

		if (pi.getCurrentSymbol() == -1) cout << "· 无待归约符号";
		else
			cout << " 待归约符号:" << getSymbol(pi.getCurrentSymbol());
		cout << " 预测符:";
		for (auto& i : pi.getSearchSym()) {
			cout << getSymbol(i) << " ";
		}
		cout << "\n";
	}
	
	cout << "\n--------------------------------------------------\n";
}
void PrintItem(LR1Item& item)
{
	cout << "输出LR(1)项目：\n";
	cout << "产生式号：" << item.getProducerNum() << endl;
	cout << producerVector[item.getProducerNum()].left << "->";
	for (int i = 0; i < producerVector[item.getProducerNum()].right.size(); i++)
		cout << producerVector[item.getProducerNum()].right[i] << "  ";
	cout << "\n预测符为：";
	for (auto& i : item.getSearchSym())
		cout << i << ",";
	cout << "\n--------------------------------------------------\n";
}
