%{

%}
%token <yyval> IDENTIFIER CONSTANT IF LE_OP GE_OP EQ_OP INT DOUBLE VOID BOOL FLOAT CHAR RETURN
%type <yyval> representation F T G op type
%left '+' '-' 
%left '*' '/' 
%left '(' ')'
%%
S : function_definition
| S function_definition
;

function_definition : type IDENTIFIER '(' variables ')' '{' block '}'
| type IDENTIFIER '(' ')' '{' block '}'
;

variables : type IDENTIFIER ',' variables
| type IDENTIFIER
;

block : allexpression
| block allexpression
;

allexpression : expression
| if_expression
| return_expression
;

return_expression : RETURN representation ';'
;

compare_expression : representation op representation
;

op : '>'
| '<'
| LE_OP
| GE_OP
| EQ_OP
;

type : INT
| DOUBLE
| VOID
| BOOL
| FLOAT
| CHAR
;

if_expression : IF '(' compare_expression ')' allexpression
| IF '(' compare_expression ')' '{' block '}'
;

expression : IDENTIFIER '=' representation ';'
| type IDENTIFIER '=' representation ';'
;

representation : T
|representation A T
;

F : '(' representation ')'
|G
|'-' G
|'-' '(' representation ')'
;

T : F
|T M F
;

A : '+'
|'-'
;

M : '*'
|'/'
;

G : IDENTIFIER
| CONSTANT
| function_use
;

function_use : IDENTIFIER '(' ')'
| IDENTIFIER '(' newvariables ')'
;

newvariables : representation
| newvariables ',' representation
;
%%

void test()
{
	//do nothing
}