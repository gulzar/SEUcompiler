#include<iostream>
#include<fstream>
#include<string>
#include <regex>
using namespace std;

struct Pair
{
	string first;
	string second;
	Pair* next = NULL;
};
struct Node;
struct Line
{
	string str;
	Node* nextNode;
	Line* next = NULL;
};
struct Node
{
	int id;
	Line* line;
	string result = "";
	Node* next = NULL;
	bool* NFANodeGroup;
};
struct Symbol
{
	string sym;
	bool* NFANodeGroup;
	Symbol* next;
};

Pair* pairs = NULL;
Node* NFA = NULL;
Symbol* symbols = NULL;
Node* DFA = NULL;
ifstream ifile;
ofstream ofile;
int NFANodeCount = 0;

Pair* newRule(string first, string second);
Node* newNFANode();
Line* newLine(string str, Node* nextNode, Node* nodeToAdd);
void makeNFA();
void processLine(Node* node, Line* line);
void newSymbol(string sym);
void symbolIdReset();
bool equalGroup(bool* ga, bool* gb);
void newDFANode(bool* NFANodeGroup);
Node* DFANode(bool* NFANodeGroup);
bool has(bool* NFANodeGroup, int NFANodeId);
void extend(bool* NFANodeGroup);
void makeDFATable(Node* DFAnode);
void NFA2DFA();
void minDFA();
void print();

void main()
{
	char name[] = "c99.l";
	cout << "Please input the file name:";
	cin >> name;
	ifile.open(name, ios::in);
	cout << "Analysing source......" << endl;
	ofile.open("yylex.cpp", ios::out);
	if (!ifile.good())
	{
		cerr << "Open the file error!" << endl;
		return;
	}
	char c1 = ifile.get();
	char c2 = ifile.get();
	if (c1 != '%' || c2 != '{')
	{
		cout << "The input file has no correct formation,Please try again!\n";
		return;
	}
	//判断到%}或到文件尾为止进行扫描
	while (!ifile.eof())
	{
		c1 = ifile.get();
		c2 = ifile.get();
		if (c1 == '%' && c2 == '}')
			break;
		else
		{
			ifile.unget();
			ifile.unget();
		}
		c2 = ifile.get();
		if (c2 == '\t') continue;//跳过\t字符不输出。
		ofile.put(c2);
	}
	//以上完成定义段中%{ %}之间的扫描
	////////////////////////////////////////////////////////////////////////////////////////////
	//以下开始对定义好的关键字的正规表达式的扫描，并将其存储到表中
	ifile.get();
	while (!ifile.eof())
	{
		c1 = ifile.get();
		c2 = ifile.get();
		if (c1 == '%' && c2 == '%')
			break;
		else
		{
			ifile.unget();
			ifile.unget();
		}
		string id, re;
		ifile >> id >> re;
		newRule(id, re);
		ifile.get();
	}
	newRule(".", "[ -~]");
	//以上是对定义段的扫描
	//以下是对规则段的扫描解析
	NFANodeCount = 0;
	newNFANode();
	ifile.get();
	while (!ifile.eof())
	{
		c1 = ifile.get();
		c2 = ifile.get();
		if (c1 == '%' && c2 == '%')
			break;
		else
		{
			ifile.unget();
			ifile.unget();
		}
		string id, re;
		getline(ifile, re);
		int count = 1;
		int i;
		for (i = re.size() - 2; i >= 0; i--)
		{
			if (re[i] == '\t')
				break;
		}
		int j = i;
		i++;
		while (re[j] == ' ' || re[j] == '\t')
			j--;
		id = re.substr(0, j + 1);
		re = re.substr(i);
		Node* no = newNFANode();
		no->result = re;
		newLine(id, no, NFA);
	}

	//将所有符号替换为正规式
	for (Line* i = NFA->line; i != NULL;)
	{
		string str = i->str;
		int j = 0;
		while (str[j] != '{' && str[j] != '\0')
			j++;
		if (str[j] == '\0')
		{
			i = i->next;
			continue;
		}
		int k = j + 1, l = 1;
		while (str[k] != '\0')
		{
			if (str[k] == '{')
				l++;
			if (str[k] == '}')
				l--;
			if (l == 0)
				break;
			k++;
		}
		if (l != 0)
		{
			i = i->next;
			continue;
		}
		string str1 = str.substr(0, j);
		string str2 = str.substr(j + 1, k - j - 1);
		string str3 = str.substr(k + 1);
		for (Pair* p = pairs; p != NULL; p = p->next)
		{
			if (str2 == p->first)
			{
				str2 = p->second;
				break;
			}
		}
		str = str1 + str2 + str3;
		i->str = str;
	}
	makeNFA();
	for (Node* i = NFA; i != NULL; i = i->next)
		for (Line* j = i->line; j != NULL; j = j->next)
			newSymbol(j->str);
	NFA2DFA();
	minDFA();
	print();

	//下面开始是最后一段，即用户自定义子例程段的输出。
	char c = 1;
	while ((c = ifile.get()) != -1)
	{
		ofile.put(c);
	}
	ifile.close();
	ofile.close();
	cout << "Done!" << endl;
}

Pair* newRule(string first, string second)
{
	Pair* pi = new Pair;
	pi->first = first;
	pi->second = second;
	if (pairs == NULL)
		pairs = pi;
	else
	{
		Pair* i = pairs;
		while (i->next != NULL)
			i = i->next;
		i->next = pi;
	}
	return pi;
}

Node* newNFANode()
{
	Node* no = new Node;
	no->line = NULL;
	if (NFA == NULL)
	{
		no->id = 0;
		NFA = no;
	}
	else
	{
		int i = 1;
		Node* j = NFA;
		while (j->next != NULL)
		{
			j = j->next;
			i++;
		}
		no->id = i;
		j->next = no;
	}
	NFANodeCount = no->id + 1;
	return no;
}

Line* newLine(string str, Node* nextNode, Node* nodeToAdd)
{
	Line* li = new Line;
	li->nextNode = nextNode;
	li->str = str;
	if (nodeToAdd->line == NULL)
		nodeToAdd->line = li;
	else
	{
		Line* i = nodeToAdd->line;
		while (i->next != NULL)
			i = i->next;
		i->next = li;
	}
	return li;
}

void makeNFA()
{
	for (Line* i = NFA->line; i != NULL; i = i->next)
		processLine(NFA, i);
}

void processLine(Node* node, Line* line)
{
	string str = line->str;
	if (str.size() <= 1)
		return;
	if (str[0] == '[')
	{
		int i = 1;
		while (str[i] != ']' && i < str.size())
			i++;
		if (str[i] != ']')
			i = 0;
		if (i == str.size() - 1)
		{
			bool b = true;
			for (int i = 32; i < 128; i++)
			{
				string st = "";
				st += (char)i;
				if (regex_match(st, regex(str)))
				{
					if (b)
					{
						line->str = st;
						b = false;
					}
					else
						newLine(st, line->nextNode, node);
				}
			}
		}
		else
		{
			if (str[i + 1] == '*')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine(theRest, line->nextNode, newNode);
				line->nextNode = newNode;
				line->str = "";
				Line* newLine2 = newLine(processingPart, newNode, newNode);
				processLine(newNode, newLine1);
				processLine(newNode, newLine2);
			}
			else if (str[i + 1] == '?')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine("", newNode, node);
				Line* newLine2 = newLine(theRest, line->nextNode, newNode);
				line->str = processingPart;
				line->nextNode = newNode;
				processLine(node, line);
				processLine(newNode, newLine2);
			}
			else if (str[i + 1] == '|')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Line* newLine1 = newLine(processingPart, line->nextNode, node);
				line->str = theRest;
				processLine(node, line);
				processLine(node, newLine1);
			}
			else if (str[i + 1] == '+')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine(theRest, line->nextNode, newNode);
				line->nextNode = newNode;
				line->str = processingPart;
				Line* newLine2 = newLine(processingPart, newNode, newNode);
				processLine(newNode, newLine1);
				processLine(node, line);
				processLine(newNode, newLine2);
			}
			else
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 1);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine(theRest, line->nextNode, newNode);
				line->nextNode = newNode;
				line->str = processingPart;
				processLine(node, line);
				processLine(newNode, newLine1);
			}
		}
	}
	else if (str[0] == '(')
	{
		int i = 1;
		int count = 1;
		while (i < str.size() - 1)
		{
			if (str[i] == '(')
				count++;
			if (str[i] == ')')
				count--;
			if (count == 0)
				break;
			i++;
		}
		if (i == str.size() - 1)
		{
			line->str = str.substr(1, str.size() - 2);
			processLine(node, line);
		}
		else
		{
			if (str[i + 1] == '*')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine(theRest, line->nextNode, newNode);
				line->nextNode = newNode;
				line->str = "";
				Line* newLine2 = newLine(processingPart, newNode, newNode);
				processLine(newNode, newLine1);
				processLine(newNode, newLine2);
			}
			else if (str[i + 1] == '?')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine("", newNode, node);
				Line* newLine2 = newLine(theRest, line->nextNode, newNode);
				line->str = processingPart;
				line->nextNode = newNode;
				processLine(node, line);
				processLine(newNode, newLine2);
			}
			else if (str[i + 1] == '|')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Line* newLine1 = newLine(processingPart, line->nextNode, node);
				line->str = theRest;
				processLine(node, line);
				processLine(node, newLine1);
			}
			else if (str[i + 1] == '+')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine(theRest, line->nextNode, newNode);
				line->nextNode = newNode;
				line->str = processingPart;
				Line* newLine2 = newLine(processingPart, newNode, newNode);
				processLine(newNode, newLine1);
				processLine(node, line);
				processLine(newNode, newLine2);
			}
			else
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 1);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine(theRest, line->nextNode, newNode);
				line->nextNode = newNode;
				line->str = processingPart;
				processLine(newNode, newLine1);
				processLine(node, line);
			}
		}
	}
	else if (str[0] == '\"')
	{
		int i = 1;
		while (str[i] != '\"')
			i++;
		if (i == str.size() - 1)
		{
			str = str.substr(1, str.size() - 2);
			line->str = str.substr(0, 1);
			if (str.size() != 1)
			{
				Node* newNode = newNFANode();
				Line* newLine1 = newLine("\"" + str.substr(1) + "\"", line->nextNode, newNode);
				line->nextNode = newNode;
				processLine(newNode, newLine1);
			}
		}
		else
		{
			if (str[i + 1] == '*')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine(theRest, line->nextNode, newNode);
				line->nextNode = newNode;
				line->str = "";
				Line* newLine2 = newLine(processingPart, newNode, newNode);
				processLine(newNode, newLine1);
				processLine(newNode, newLine2);
			}
			else if (str[i + 1] == '?')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine("", newNode, node);
				Line* newLine2 = newLine(theRest, line->nextNode, newNode);
				line->str = processingPart;
				line->nextNode = newNode;
				processLine(node, line);
				processLine(newNode, newLine2);
			}
			else if (str[i + 1] == '|')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Line* newLine1 = newLine(processingPart, line->nextNode, node);
				line->str = theRest;
				processLine(node, line);
				processLine(node, newLine1);
			}
			else if (str[i + 1] == '+')
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 2);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine(theRest, line->nextNode, newNode);
				line->nextNode = newNode;
				line->str = processingPart;
				Line* newLine2 = newLine(processingPart, newNode, newNode);
				processLine(newNode, newLine1);
				processLine(node, line);
				processLine(newNode, newLine2);
			}
			else
			{
				string processingPart = str.substr(0, i + 1);
				string theRest = str.substr(i + 1);
				Node* newNode = newNFANode();
				Line* newLine1 = newLine(theRest, line->nextNode, newNode);
				line->nextNode = newNode;
				line->str = processingPart;
				processLine(newNode, newLine1);
				processLine(node, line);
			}
		}
	}
	else if (str[0] == '\\')
	{
		string processingPart = "\\";
		if (str[1] == '\\')
			processingPart = "\\";
		else if (str[1] == '\"')
			processingPart = "\"";
		else if (str[1] == '\'')
			processingPart = "\'";
		else if (str[1] == 't')
			processingPart = "\t";
		else if (str[1] == 'v')
			processingPart = "\v";
		else if (str[1] == 'n')
			processingPart = "\n";
		else if (str[1] == 'f')
			processingPart = "\f";
		else if (str[1] == '0')
			processingPart = "\0";
		else if (str[1] == '?')
			processingPart = "\?";
		else if (str[1] == 'r')
			processingPart = "\r";
		else if (str[1] == 'a')
			processingPart = "\a";
		else if (str[1] == 'b')
			processingPart = "\b";
		else
			exit(0);
		if (str[2] == '*')
		{
			string theRest = str.substr(3);
			Node* newNode = newNFANode();
			Line* newLine1 = newLine(theRest, line->nextNode, newNode);
			line->nextNode = newNode;
			line->str = "";
			Line* newLine2 = newLine(processingPart, newNode, newNode);
			processLine(newNode, newLine1);
			processLine(newNode, newLine2);
		}
		else if (str[2] == '?')
		{
			string theRest = str.substr(3);
			Node* newNode = newNFANode();
			Line* newLine1 = newLine("", newNode, node);
			Line* newLine2 = newLine(theRest, line->nextNode, newNode);
			line->str = processingPart;
			line->nextNode = newNode;
			processLine(node, line);
			processLine(newNode, newLine2);
		}
		else if (str[2] == '|')
		{
			string theRest = str.substr(3);
			Line* newLine1 = newLine(processingPart, line->nextNode, node);
			line->str = theRest;
			processLine(node, line);
		}
		else if (str[2] == '+')
		{
			string theRest = str.substr(3);
			Node* newNode = newNFANode();
			Line* newLine1 = newLine(theRest, line->nextNode, newNode);
			line->nextNode = newNode;
			line->str = processingPart;
			Line* newLine2 = newLine(processingPart, newNode, newNode);
			processLine(newNode, newLine1);
		}
		else
		{
			string theRest = str.substr(2);
			Node* newNode = newNFANode();
			Line* newLine1 = newLine(theRest, line->nextNode, newNode);
			line->nextNode = newNode;
			line->str = processingPart;
			processLine(newNode, newLine1);
		}
	}
	else
	{
		if (str[1] == '*')
		{
			string processingPart = str.substr(0, 1);
			string theRest = str.substr(2);
			Node* newNode = newNFANode();
			Line* newLine1 = newLine(theRest, line->nextNode, newNode);
			line->nextNode = newNode;
			line->str = "";
			Line* newLine2 = newLine(processingPart, newNode, newNode);
			processLine(newNode, newLine1);
			processLine(newNode, newLine2);
		}
		else if (str[1] == '?')
		{
			string processingPart = str.substr(0, 1);
			string theRest = str.substr(2);
			Node* newNode = newNFANode();
			Line* newLine1 = newLine("", newNode, node);
			Line* newLine2 = newLine(theRest, line->nextNode, newNode);
			line->str = processingPart;
			line->nextNode = newNode;
			processLine(node, line);
			processLine(newNode, newLine2);
		}
		else if (str[1] == '|')
		{
			string processingPart = str.substr(0, 1);
			string theRest = str.substr(2);
			Line* newLine1 = newLine(processingPart, line->nextNode, node);
			line->str = theRest;
			processLine(node, line);
		}
		else if (str[1] == '+')
		{
			string processingPart = str.substr(0, 1);
			string theRest = str.substr(2);
			Node* newNode = newNFANode();
			Line* newLine1 = newLine(theRest, line->nextNode, newNode);
			line->nextNode = newNode;
			line->str = processingPart;
			Line* newLine2 = newLine(processingPart, newNode, newNode);
			processLine(newNode, newLine1);
		}
		else
		{
			string processingPart = str.substr(0, 1);
			string theRest = str.substr(1);
			Node* newNode = newNFANode();
			Line* newLine1 = newLine(theRest, line->nextNode, newNode);
			line->nextNode = newNode;
			line->str = processingPart;
			processLine(newNode, newLine1);
		}
	}
}

void newSymbol(string sym)
{
	if (sym == "")
		return;
	if (symbols == NULL)
	{
		Symbol* s = new Symbol;
		s->sym = sym;
		s->next = NULL;
		s->NFANodeGroup = new bool[NFANodeCount];
		for (int i = 0; i < NFANodeCount; i++)
			s->NFANodeGroup[i] = false;
		symbols = s;
	}
	else
	{
		Symbol* s2 = symbols;
		while (s2->next != NULL)
		{
			if (s2->sym == sym)
				return;
			s2 = s2->next;
		}
		if (s2->sym == sym)
			return;
		Symbol* s = new Symbol;
		s->sym = sym;
		s->next = NULL;
		s->NFANodeGroup = new bool[NFANodeCount];
		for (int i = 0; i < NFANodeCount; i++)
			s->NFANodeGroup[i] = false;
		s2->next = s;
	}
}

void symbolIdReset()
{
	Symbol* s = symbols;
	while (s != NULL)
	{
		for (int i = 0; i < NFANodeCount; i++)
			s->NFANodeGroup[i] = false;
		s = s->next;
	}
}

bool equalGroup(bool* ga, bool* gb)
{
	for (int i = 0; i < NFANodeCount; i++)
		if (ga[i] != gb[i])
			return false;
	return true;
}

void newDFANode(bool* NFANodeGroup)
{
	if (DFA == NULL)
	{
		Node* no = new Node;
		no->line = NULL;
		no->id = 0;
		DFA = no;
		no->NFANodeGroup = new bool[NFANodeCount];
		for (int i = 0; i < NFANodeCount; i++)
			no->NFANodeGroup[i] = NFANodeGroup[i];
	}
	else
	{
		Node* n = DFA;
		int i = 1;
		while (n->next != NULL)
		{
			if (equalGroup(n->NFANodeGroup, NFANodeGroup))
				return;
			n = n->next;
			i++;
		}
		if (equalGroup(n->NFANodeGroup, NFANodeGroup))
			return;
		Node* no = new Node;
		no->line = NULL;
		no->id = i;
		n->next = no;
		no->NFANodeGroup = new bool[NFANodeCount];
		for (int i = 0; i < NFANodeCount; i++)
			no->NFANodeGroup[i] = NFANodeGroup[i];
	}
}

Node* DFANode(bool* NFANodeGroup)
{
	if (DFA == NULL)
		return NULL;
	else
	{
		Node* n = DFA;
		while (n != NULL)
		{
			if (equalGroup(n->NFANodeGroup, NFANodeGroup))
				return n;
			n = n->next;
		}
		return NULL;
	}
}

bool has(bool* NFANodeGroup, int NFANodeId)
{
	return NFANodeGroup[NFANodeId];
}

void extend(bool* NFANodeGroup)
{
	bool changed = true;
	while (changed)
	{
		changed = false;
		for (Node* n = NFA; n != NULL; n = n->next)
			if (has(NFANodeGroup, n->id))
				for (Line* l = n->line; l != NULL; l = l->next)
					if (l->str == "")
						if (!has(NFANodeGroup, l->nextNode->id))
						{
							NFANodeGroup[l->nextNode->id] = true;
							changed = true;
						}
	}
}

void makeDFATable(Node* DFAnode)
{
	for (Node* n = NFA; n != NULL; n = n->next)
		if (has(DFAnode->NFANodeGroup, n->id))
			for (Line* l = n->line; l != NULL; l = l->next)
				for (Symbol* s = symbols; s != NULL; s = s->next)
					if (l->str == s->sym)
						s->NFANodeGroup[l->nextNode->id] = true;
	for (Symbol* s = symbols; s != NULL; s = s->next)
	{
		extend(s->NFANodeGroup);
		bool allFalse = true;
		for (int i = 0; i < NFANodeCount; i++)
			allFalse = (allFalse && !s->NFANodeGroup[i]);
		if (!allFalse)
		{
			newDFANode(s->NFANodeGroup);
			newLine(s->sym, DFANode(s->NFANodeGroup), DFANode(DFAnode->NFANodeGroup));
		}
	}
	symbolIdReset();
}

void NFA2DFA()
{
	bool* firstGroup = new bool[NFANodeCount];
	for (int i = 0; i < NFANodeCount; i++)
		firstGroup[i] = false;
	firstGroup[0] = true;
	extend(firstGroup);
	newDFANode(firstGroup);
	makeDFATable(DFANode(firstGroup));
	for (Node* n = DFA->next; n != NULL; n = n->next)
		makeDFATable(n);
	for (Node* n = NFA; n != NULL; n = n->next)
		if (n->result != "")
			for (Node* n2 = DFA; n2 != NULL; n2 = n2->next)
				if (has(n2->NFANodeGroup, n->id) && n2->result == "")
					n2->result = n->result;
}

void minDFA()
{

}

void print()
{
	ofile << "using namespace std;\n"
		<< "string analysis(char *yytext,int n)\n"
		<< "{\n"
		<< "\tint state = 0;\n"
		<< "\tint N = n + 1;//N表示串长加1,为与状态数保持一致。\n"
		<< "\tfor (int i = 0; i < N; i++)\n"
		<< "\t{\n"
		<< "\t\tswitch (state)\n"
		<< "\t\t{\n";
	for (Node* n = DFA; n != NULL; n = n->next)
	{
		ofile << "\t\t\tcase " << n->id << ":\n"
			<< "\t\t\t{\n";
		if (n->result != "")
		{
			ofile << "\t\t\t\tif(i==N-1)\n"
				<< "\t\t\t\t{\n";
			ofile << "\t\t\t\t\t" << n->result.substr(1, n->result.size() - 2) << "\n";
			ofile << "\t\t\t\t\tbreak;\n"
				<< "\t\t\t\t}\n";
		}
		if (n->line == NULL)
			ofile << "\t\t\t\treturn \"ERROR\";\n";
		else
		{
			if (n->line->str == "\a")
				n->line->str = "\\a";
			if (n->line->str == "\b")
				n->line->str = "\\b";
			if (n->line->str == "\r")
				n->line->str = "\\r";
			if (n->line->str == "\?")
				n->line->str = "\\?";
			if (n->line->str == "\n")
				n->line->str = "\\n";
			if (n->line->str == "\t")
				n->line->str = "\\t";
			if (n->line->str == "\v")
				n->line->str = "\\v";
			if (n->line->str == "\f")
				n->line->str = "\\f";
			if (n->line->str == "\"")
				n->line->str = "\\\"";
			if (n->line->str == "\'")
				n->line->str = "\\\'";
			if (n->line->str == "\\")
				n->line->str = "\\\\";
			if (n->line->str == "\0")
				n->line->str = "\\0";
			ofile << "\t\t\t\tif (yytext[i] == '" << n->line->str[0];
			if (n->line->str[0] == '\\')
				ofile << n->line->str[1];
			ofile << "')\n"
				<< "\t\t\t\t{\n"
				<< "\t\t\t\t\tstate=" << n->line->nextNode->id << ";\n"
				<< "\t\t\t\t\tbreak;\n"
				<< "\t\t\t\t}\n";
			for (Line* l = n->line->next; l != NULL; l = l->next)
			{
				if (l->str == "\a")
					l->str = "\\a";
				if (l->str == "\b")
					l->str = "\\b";
				if (l->str == "\r")
					l->str = "\\r";
				if (l->str == "\?")
					l->str = "\\?";
				if (l->str == "\n")
					l->str = "\\n";
				if (l->str == "\t")
					l->str = "\\t";
				if (l->str == "\f")
					l->str = "\\f";
				if (l->str == "\v")
					l->str = "\\v";
				if (l->str == "\"")
					l->str = "\\\"";
				if (l->str == "\'")
					l->str = "\\\'";
				if (l->str == "\\")
					l->str = "\\\\";
				if (l->str == "\0")
					l->str = "\\0";
				ofile << "\t\t\t\telse if (yytext[i] == '" << (char)(int)l->str[0];
				if (l->str[0] == '\\')
					ofile << l->str[1];
				ofile << "')\n"
					<< "\t\t\t\t{\n"
					<< "\t\t\t\t\tstate=" << l->nextNode->id << ";\n"
					<< "\t\t\t\t\tbreak;\n"
					<< "\t\t\t\t}\n";
			}
			ofile << "\t\t\t\telse\n"
				<< "\t\t\t\t{\n"
				<< "\t\t\t\t\treturn \"ERROR\";\n"
				<< "\t\t\t\t}\n"
				<< "\t\t\t\tbreak;\n";
		}
		ofile << "\t\t\t}\n";
	}
	ofile << "\t\t}\n"
		<< "\t}\n"
		<< "}\n";
}